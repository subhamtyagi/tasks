package com.mmt.shubh.owsmtasks.ui.mvpviews;

/**
 * Created by shubham on 1/6/16.
 */
public interface SplashView extends MvpView {
    void startHomeActivity();

    void showError(String s);
}
